Implement a class PowerUnit, which stores 
* id (`int`), 
* heat level (`int`) 
* heat level threshold (`int`). 

PowerUnit should trigger an event whenever heat level is set equal to or above the heat threshold, passing id and current heat level as event args.

Implement a console application, which does the following:
* prompts user for number of power units and heat threshold
* creates power units with `0` heat
* each time user clicks "Enter", system increases heat level of each power unit for a random value

Whenever overheat event is triggered, system should notify user about it (write info to console) and reset overheated power unit (set heat level back to `0`).
